class UpdateUserMembershipJob < ApplicationJob
  queue_as :default

  def perform(user)
    membership_status = MembershipService.get_membership_status(user)
    if user.member? != membership_status
      user.member = membership_status
      user.save
    end
  end
end
