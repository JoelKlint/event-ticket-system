module ApplicationHelper

  def format_time(time)
    begin
      return time.strftime('%e %b %Y %H:%M')
    rescue
      return "Undefined"
    end
  end

  def current_user
    user = super
    user ||= User.new
  end

end
