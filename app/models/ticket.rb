class Ticket < ApplicationRecord

  belongs_to :event
  validates :event, presence: true

  belongs_to :user
  validates :user, presence: true

end
