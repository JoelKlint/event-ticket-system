class User < ApplicationRecord
  after_update :update_membership_status, if: :ssn_changed?
  before_update :reset_membership_status, if: :ssn_changed?

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :attending_events, through: :tickets, source: :event
  has_many :tickets, dependent: :destroy

  validates :ssn,
    numericality: { only_integer: true },
    allow_nil: true,
    uniqueness: true,
    length: { is: 10 }


  def attending?(event)
    attending_events.exists?(event.id)
  end

  private

  def reset_membership_status
    self.member = false
  end

  def update_membership_status
    UpdateUserMembershipJob.perform_later(User.find(self.id))
  end

end
