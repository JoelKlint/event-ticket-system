class Event < ApplicationRecord

  has_many :attendees, through: :tickets, source: :user
  has_many :tickets, dependent: :destroy

  validates :title, presence: true
  validates :start_time, presence: true
  validates :end_time, presence: true

  validate :end_time_must_be_after_start_time

  def end_time_must_be_after_start_time
    return unless start_time and end_time
    errors.add(:end_time, "Must be after start time") if start_time > end_time
  end

end
