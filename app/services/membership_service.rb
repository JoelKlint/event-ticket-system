class MembershipService

  def self.get_membership_status(user)

    query = {
      "id" => user.ssn,
      "apikey" => Rails.application.secrets.medcheck_api_key,
    }
    headers = {
      "Accept" => "application/json",
    }
    uri = 'http://medlem.tlth.se/api/getstudent'

    response = HTTParty.get(uri, query: query, headers: headers)

    if response.success?
      body = JSON.parse(response.body)
      is_member = body['getstudent']['betalningar'].last['betald']
      return is_member
    else
      return false
    end
  end

end

