class EventsController < ApplicationController
  load_and_authorize_resource

  def new
  end

  def index
  end

  def create
    if @event.save
      redirect_to @event
    else
      render :new
    end
  end

  def show
  end

  def tickets
  end

  private

  def event_params
    params.require(:event).permit(:title, :start_time, :end_time)
  end
end
