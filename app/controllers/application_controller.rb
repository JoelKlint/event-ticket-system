class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from CanCan::AccessDenied do |exception|
    flash[:alert] = "You are not allowed to be there"
    redirect_to root_url
  end

  before_action :redirect_to_ssn_if_unset
  skip_before_action :redirect_to_ssn_if_unset, if: :devise_controller?

  def redirect_to_ssn_if_unset
    if user_signed_in? && current_user.ssn.nil?
      flash[:notice] = "We need you social security number in order to sell you tickets"
      redirect_to set_ssn_user_path(current_user)
    end
  end

end
