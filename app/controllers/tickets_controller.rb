class TicketsController < ApplicationController
  load_and_authorize_resource

  def create
    @ticket.user = current_user
    if @ticket.save
      redirect_to @ticket.event
    else
      flash[:alert] = "Could not book ticket"
      redirect_to events_path
    end
  end

  def destroy
    redirect_to(event_path(@ticket.event)) if @ticket.destroy
  end

  private

  def ticket_params
    params.require(:ticket).permit(:event_id)
  end
end
