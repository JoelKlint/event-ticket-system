class UsersController < ApplicationController
  skip_before_action :redirect_to_ssn_if_unset, only: [:set_ssn, :save_ssn]
  load_and_authorize_resource

  def show
  end

  def update
    if @user.update_attributes(update_params)
      flash[:success] = "Saved new info"
      redirect_to user_path(@user)
    else
      render :show
    end
  end

  def set_ssn
  end

  def save_ssn
    if @user.update_attributes(save_ssn_params)
      flash[:success] = "Saved SSN"
      redirect_to user_path(@user)
    else
      render :set_ssn
    end
  end

  private

  def update_params
    params.require(:user).permit(:first_name, :last_name, :food_preferences)
  end

  def save_ssn_params
    params.require(:user).permit(:ssn)
  end

end
