# Development
## Dependencies
- Postgresql
- Redis

## Install project
1. Use appropiate ruby version, see .ruby-version
2. Run this in shell
```shell
bundle install
rake db:create && rake db:migrate && rake db:seed
gem install foreman
gem install mailcatcher

# Creates development environment variables
echo 'MEDCHECK_API_KEY=VARIABLE' >> .env
echo 'RACK_ENV=development' >> .env
echo 'RAILS_ENV=development' >> .env
echo 'PORT=3000' >> .env

# Copies production database
heroku pg:backups:capture --app event-tickets-system && heroku pg:backups:download --app event-tickets-system
pg_restore --verbose --clean --no-acl --no-owner -d event_ticket_system_dev latest.dump
```
3. Replace all instances of ```VARIABLE``` with something appropiate in ```.env```

## During development
1. Start server with ```foreman start```
1. run ```bundle exec guard``` to run tests continuously
2. TDD like there is no tomorrow

## If you want to test mails, do this
1. run ```mailcatcher``` and open [http://127.0.0.1:1080](http://127.0.0.1:1080) in web browser. All mail will be delivered here

# Tests
1. To get code coverage, run ```rspec spec``` in shell and open ```coverage/index.html```

# Environment variables
- SECRET_KEY_BASE : verifies signed cookies.
- SMTP_USERNAME : username for smtp server that should send email (gmail only)
- SMTP_PASSWORD : password for smtp server that should send email (gmail only)
- DATABASE_URL : url to database to use
- DOMAIN_NAME : domain name where app is hosted
- ROLLBAR_ACCESS_TOKEN : used to track errors with rollbar
- MEDCHECK_API_KEY : used to access membership api
- REDIS_URL : url to redis database to use


# Rails Readme

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Database initialization

* Services (job queues, cache servers, search engines, etc.)

* Deployment instructions

* ...
