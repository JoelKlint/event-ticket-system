class AddFoodPrefsToUser < ActiveRecord::Migration[5.0]
  def change

    add_column :users, :food_preferences, :string

  end
end
