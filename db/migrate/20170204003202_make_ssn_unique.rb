class MakeSsnUnique < ActiveRecord::Migration[5.0]
  def change
    
    add_index :users, :ssn, unique: true

  end
end
