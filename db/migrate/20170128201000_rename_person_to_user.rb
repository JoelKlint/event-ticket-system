class RenamePersonToUser < ActiveRecord::Migration[5.0]
  def change
    rename_table :people, :users
    rename_column :tickets, :person_id, :user_id
  end
end
