class MakeTicketsUnique < ActiveRecord::Migration[5.0]
  def change

    add_index :tickets, [:user_id, :event_id], unique: true

  end
end
