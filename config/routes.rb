Rails.application.routes.draw do
  devise_for :users
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root 'events#index'

  resources :events do
    member do
      get :tickets
    end
  end

  resources :tickets, only: [:create, :destroy]

  resources :users, only: [:show, :update] do
    member do
      get :set_ssn
      post :save_ssn
    end
  end

end
