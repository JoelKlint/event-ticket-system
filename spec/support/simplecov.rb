if ENV["RUN_COVERAGE"] != 'false'
  require 'simplecov'
  SimpleCov.start 'rails' do
    add_group 'Services', 'app/services'
  end
end
