RSpec.configure do |config|

  config.before :each do |test|
    if test.metadata[:type] == :view
      allow_any_instance_of(ActionController::TestRequest).to receive(:original_url).and_return('')
    end
  end

end
