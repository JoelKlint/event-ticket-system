require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

dir = File.dirname(__FILE__)
File.read(dir + '/external-http-mocks/medlem.tlth.se.json')

RSpec.configure do |config|
  config.before(:each) do
    stub_request(:get, /medlem.tlth.se/ )
      .with(headers: {
        'Accept'=>'application/json'
      })
      .to_return(
        status: 200,
        body: File.read(dir + '/external-http-mocks/medlem.tlth.se.json'),
        headers: {}
      )
  end
end
