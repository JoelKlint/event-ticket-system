require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before :each do
    @user = create(:user)
    sign_in @user
  end

  context "show" do

    it "renders 'show' template" do
      get :show, params: { id: @user.id }
      expect(response).to render_template(:show)
    end
  end

  context "update" do

    it "does not allow changing ssn" do
      pre_ssn = @user.ssn
      attrs = attributes_for(:user)
      put :update, params: { id: @user.id, user: attrs }
      @user.reload
      expect(@user.ssn).to eq(pre_ssn)
    end

    it "allows changing first_name and last_name" do
      pre_first_name = @user.first_name
      pre_last_name = @user.last_name
      attrs = attributes_for(:user, first_name: 'new', last_name: 'name')
      put :update, params: { id: @user.id, user: attrs }
      @user.reload
      expect(@user.first_name).to eq(attrs[:first_name])
      expect(@user.last_name).to eq(attrs[:last_name])
    end

    it "allows setting food_prefs" do
      attrs = attributes_for(:user, food_preferences: 'Cucumber')
      put :update, params: { id: @user.id, user: attrs }
      @user.reload
      expect(@user.food_preferences).to eq(attrs[:food_preferences])
    end

    it "redirects to user and sets the flash on success" do
      attrs = attributes_for(:user)
      put :update, params: { id: @user.id, user: attrs }
      expect(flash[:success]).to be_present
    end

  end

  context "#set_ssn" do
    it "renders the 'set_ssn' template" do
      get :set_ssn, params: { id: @user.id }
      expect(response).to render_template(:set_ssn)
    end
  end

  context "#save_ssn" do
    it "redirects to user profile on success" do
      ssn = attributes_for(:user)[:ssn]
      expect(@user.ssn).not_to eq(ssn)
      post :save_ssn, params: { id: @user.id, user: { ssn: ssn }}
      @user.reload
      expect(@user.ssn).to eq(ssn)
      expect(response).to redirect_to(user_path(@user))
      expect(flash[:success]).to be_present
    end

    it "renders 'set_ssn' template on failure" do
      pre_ssn = @user.ssn
      ssn = 'abc'
      expect(@user.ssn).not_to eq(ssn)
      post :save_ssn, params: { id: @user.id, user: { ssn: ssn }}
      @user.reload
      expect(@user.ssn).to eq(pre_ssn)
      expect(response).to render_template(:set_ssn)
    end
  end

end
