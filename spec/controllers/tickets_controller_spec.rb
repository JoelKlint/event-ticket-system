require 'rails_helper'

RSpec.describe TicketsController, type: :controller do

  context "#create" do

    before :each do
      @user = create(:user)
      sign_in @user

      event = create(:event)
      @ticket = attributes_for(:ticket, event_id: event.id)
    end

    it "redirects to event on success" do
      post :create, params: { ticket: @ticket }
      expect(response).to redirect_to(event_path(@ticket[:event_id]))
      expect(response).not_to render_template(:new)
    end

    it "creates the ticket to the logged in user" do
      post :create, params: { ticket: @ticket }
      ticket = assigns(:ticket)
      expect(ticket.user).to eq(@user)
    end

    it "redirects to events on fail and sets the alert flash" do
      post :create, params: { ticket: {:event_id => nil} }
      expect(response).to redirect_to(events_path)
      expect(flash[:alert]).to be_present
    end

  end

  context "#destroy" do

    it "redirects to event on success" do
      @user = create(:user, :with_tickets)
      sign_in @user
      @ticket = @user.tickets.first

      delete :destroy, params: { id: @ticket.id }
      expect(response).to redirect_to(event_path(@ticket.event_id))
    end
  end

end
