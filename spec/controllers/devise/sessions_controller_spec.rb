require 'rails_helper'

RSpec.describe Devise::SessionsController, type: :controller do

  it "requires email" do
    attrs = attributes_for(:user_signup)
    attrs[:email] = nil
    post :create, params: { user: attrs }
    expect(response).not_to be_redirect
    expect(response).to render_template(:new)
  end

  it "requires password" do
    attrs = attributes_for(:user_signup)
    attrs[:password] = nil
    post :create, params: { user: attrs }
    expect(response).not_to be_redirect
    expect(response).to render_template(:new)
  end

end
