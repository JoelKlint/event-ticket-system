require 'rails_helper'

RSpec.describe Devise::RegistrationsController, type: :controller do

  it "is possible to sign up with valid parameters" do
    post :create, params: { user: attributes_for(:user_signup) }
    expect(response).to be_redirect
    expect(response).not_to render_template(:new)
  end

  it "requires an email" do
    attrs = attributes_for(:user_signup)
    attrs[:email] = nil
    post :create, params: { user: attrs }
    expect(response).not_to be_redirect
    expect(response).to render_template(:new)
  end

  it "requires password" do
    attrs = attributes_for(:user_signup)
    attrs[:password] = nil
    post :create, params: { user: attrs }
    expect(response).not_to be_redirect
    expect(response).to render_template(:new)
  end

  it "requires a matching password and password_confirmation" do
    attrs = attributes_for(:user_signup)
    attrs[:password_confirmation] = "somethine else"
    post :create, params: { user: attrs }
    expect(response).not_to be_redirect
    expect(response).to render_template(:new)
  end

end
