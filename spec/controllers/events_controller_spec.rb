require 'rails_helper'

RSpec.describe EventsController, type: :controller do

  before :each do
    @user = create(:admin)
    sign_in @user
  end

  context "#new" do
    it "is successful" do
      get :new
      expect(response).to be_successful
    end

    it "creates an event" do
      get :new
      expect(assigns(:event)).to be_a_new(Event)
    end
  end

  context "#create" do
    it "redirects to #show on success" do
      post :create, params: { event: attributes_for(:event) }
      expect(response).to redirect_to(assigns(:event))
    end

    it "renders 'new' template on failure" do
      post :create, params: { event: attributes_for(:event, :invalid) }
      expect(response).to render_template(:new)
    end
  end

  context "#show" do

    before :all do
      @event = create :event
    end

    it "is succesful for existing events" do
      get :show, params: { id: @event.id }
      expect(response).to be_successful
    end

    it "retrieves the event from db" do
      get :show, params: { id: @event.id }
      expect(assigns(:event)).to eql(@event)
    end

  end

  context "#index" do
    it "renders 'index' template" do
      get :index
      expect(response).to render_template(:index)
    end

    it "fetches all events into @events" do
      event_count =  Event.count
      get :index
      expect(assigns(:events).count).to be(event_count)
    end
  end

  context "#tickets" do
    before :each do
      @user = create(:admin)
      sign_in @user
      @event = create(:event, attendees: [@user])
    end

    it "renders 'tickets' template" do
      get :tickets, params: { id: @event.id }
      expect(response).to render_template(:tickets)
    end

    it "assigns event to the event variable" do
      get :tickets, params: { id: @event.id }
      expect(assigns(:event)).to eq(@event)
    end
  end

end
