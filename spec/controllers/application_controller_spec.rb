require 'rails_helper'

RSpec.describe ApplicationController, type: :controller do

  controller do
    def index
      raise CanCan::AccessDenied
    end
  end

  it "redirects to root and sets the alert flash on access denied" do
    get :index
    expect(response).to redirect_to(root_path)
    expect(flash[:alert]).to be_present
  end

end

RSpec.describe ApplicationController, type: :controller do

  controller do
    def index
    end
  end

  it "redirects to set_ssn screen if ssn is unset and user logged in" do
    user = create(:user, ssn: nil)
    sign_in user
    get :index
    expect(flash[:notice]).to be_present
    expect(response).to redirect_to(set_ssn_user_path(user))
  end

end



