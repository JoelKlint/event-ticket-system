require 'rails_helper'

RSpec.describe Ticket, type: :model do

  it "only creates unique tickets" do
    event = create(:event)
    user = create(:user)
    Ticket.create(event: event, user: user)
    expect{Ticket.create(event: event, user: user)}.to raise_error(ActiveRecord::RecordNotUnique)
  end

  it "requires an event to be created" do
    ticket = build(:ticket)
    ticket.event = nil
    expect(ticket).not_to be_valid
  end

  it "requires a user to be created" do
    ticket = build(:ticket)
    ticket.user = nil
    expect(ticket).not_to be_valid
  end

end
