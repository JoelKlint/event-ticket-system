require 'rails_helper'

RSpec.describe Event, type: :model do

  it "works with correct parameters" do
    event = build :event
    expect(event).to be_valid
  end

  it "requires a start and end time" do
    event = build :event
    event.start_time = nil
    event.end_time = nil
    expect(event).not_to be_valid
  end

  it "must end after it starts" do
    event = build :event
    event.start_time = event.end_time + 10
    expect(event).not_to be_valid
  end

  it "must have a title" do
    event = build :event
    event.title = nil
    expect(event).not_to be_valid
  end

  context "attendees" do
    it "can have attendees" do
      event = create(:event, :with_attendees)
      expect(event.attendees.first).to be_instance_of(User)
    end
  end

  it "destroys all associated tickets if it is destroyed" do
    event = create(:event)
    tickets = create_list(:ticket, 5, event: event)
    expect{event.destroy}.to change{Ticket.count}.from(5).to(0)
  end

end
