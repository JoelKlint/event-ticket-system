require 'rails_helper'

RSpec.describe User, type: :model do

  it "only allows unique ssn" do
    attrs = attributes_for(:user)
    ssn = attrs[:ssn]
    User.create(attrs)
    attrs = attributes_for(:user, ssn: ssn)
    user = User.new(attrs)
    expect(user.valid?).to be false
  end

  it "only allows numbers in ssn" do
    attrs = attributes_for(:user, ssn: "invalid")
    user = User.new(attrs)
    expect(user.save).to be false
  end

  it "must have ssn of length 10" do
    user = User.new(attributes_for(:user, ssn: '123456789'))
    expect(user.valid?).to be false
    user.ssn = '1234567890'
    expect(user.valid?).to be true
  end

  it "is possible to know it if is admin or not" do
    user = build :user
    expect(user.admin?).to be false

    admin = build :admin
    expect(admin.admin?).to be true
  end

  it "works with correct parameters" do
    user = build :user
    expect(user).to be_valid
  end

  it "can figure out if it is attending an event or not" do
    user = build_stubbed(:user)
    event = build(:event)
    expect(user.attending?(event)).to be false

    user = create(:user, :with_tickets)
    event = user.attending_events.first
    expect(user.attending?(event)).to be true
  end

  it "can have food preferences" do
    user = create(:user)
    user.food_preferences = "Banana, Cucumber, Oranges"
    expect(user).to be_valid
  end

  it "can be a member" do
    user = create(:user)
    expect(user.member?).to be false
    user.member = true
    expect(user.member?).to be true
  end

  context "events" do
    it "can attend events" do
      user = create(:user, :with_tickets)
      expect(user.attending_events.first).to be_instance_of(Event)
    end
  end

  it "enqueues job to find membership status when ssn is changed" do
    user = create(:user)
    ssn = attributes_for(:user)[:ssn]
    user.ssn = ssn
    expect{user.save}.to have_enqueued_job(UpdateUserMembershipJob)
  end

  it "resets membership status to false on ssn change" do
    user = create(:user, member: true)
    ssn = attributes_for(:user)[:ssn]
    user.ssn = ssn
    user.save
    user.reload
    expect(user.ssn).to eq(ssn)
    expect(user.member?).to be false
  end

  it "destroys all associated tickets if it is destroyed" do
    user = create(:user)
    tickets = create_list(:ticket, 5, user: user)
    expect{user.destroy}.to change{Ticket.count}.from(5).to(0)
  end

end
