require 'rails_helper'
require 'cancan/matchers'

RSpec.describe Ability, type: :model do

  context "basic user" do

    before :all do
      @user = build_stubbed(:user)
      @ability = Ability.new(@user)
    end

    it "gives the correct abilities" do
      other_user = build_stubbed(:user)

      expect(@ability).to be_able_to(:create, Ticket.new(user: @user))
      expect(@ability).not_to be_able_to(:create, Ticket.new(user: other_user))
      expect(@ability).to be_able_to(:destroy, Ticket.new(user: @user))
      expect(@ability).not_to be_able_to(:destroy, Ticket.new(user: other_user))

      expect(@ability).to be_able_to(:manage, @user)
      expect(@ability).not_to be_able_to(:manage, other_user)

      expect(@ability).to be_able_to(:read, Event)
      expect(@ability).not_to be_able_to(:manage, Event)
    end

  end

  context "admin user" do

    before :all do
      @admin = build_stubbed(:admin)
      @ability = Ability.new(@admin)
    end

    it "gives the correct abilities" do
      other_user = build_stubbed(:user)

      expect(@ability).to be_able_to(:manage, Event)

      expect(@ability).not_to be_able_to(:manage, other_user)

      expect(@ability).to be_able_to(:manage, Ticket)
    end

  end

  context "not logged in" do

    before :all do
      @ability = Ability.new(nil)
    end

    it "gives the correct abilities" do
      expect(@ability).to be_able_to(:index, Event.new)
    end

  end

end
