require 'rails_helper'

RSpec.describe "events/index", type: :view do

  before :all do
    @events = [];
    3.times do
      @events << FactoryGirl.create(:event)
    end
    assign(:events, @events)
  end

  it "renders a table with as many rows as events" do
    render
    expect(rendered).to have_selector('table tr', :count => 4)
  end

  it "makes event titles clickable links" do
    render
    event = @events.first
    expect(rendered).to have_link event.title, :href => event_path(event)
  end
end
