require 'rails_helper'

RSpec.describe "events/tickets", type: :view do

  it "renders the 'attendees_table' partial" do
    users = create_list(:user, 5)
    event = create(:event, attendees: users)
    assign(:event, event)
    render
    expect(rendered).to render_template(:partial => 'events/_attendees_table')
  end

end
