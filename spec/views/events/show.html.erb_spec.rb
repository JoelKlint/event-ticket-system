require 'rails_helper'

RSpec.describe "events/show", type: :view do

  context "user" do

    before :each do
      @user = create(:user)
      sign_in @user
    end

    it "displays the information" do
      event = create(:event)
      assign(:event, event)
      render
      expect(rendered).to have_content(event.title)
    end

    context "user has booked ticket" do
      it "displays a button to destroy the ticket" do
        event = create(:event, attendees: [@user])
        assign(:event, event)
        render
        ticket = event.tickets.first
        expect(rendered).to have_tag("form", :with => { :action => ticket_path(ticket)})
        expect(rendered).to have_selector("input[type=submit][value='Unbook ticket']")
      end
    end

    context "user has not booked ticket" do
      it "displays a button to create a ticket" do
        event = build_stubbed(:event)
        assign(:event, event)
        render
        expect(rendered).to have_tag('form', :with => { :action => tickets_path } )
        expect(rendered).to have_selector("input[type=submit][value='Book ticket']")
      end
    end

  end

  context "admin" do

    before :each do
      @admin = create(:admin)
      sign_in @admin
    end

    it "renders a link to tickets if user is admin" do
      event = create(:event)
      assign(:event, event)
      render
      expect(rendered).to have_link(:href => tickets_event_path(event))
    end

  end


end
