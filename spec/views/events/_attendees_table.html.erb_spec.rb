require 'rails_helper'

RSpec.describe "events/_attendees_table", type: :view do

  it "renders the user info" do
    users = create_list(:user, 15)
    assign(:attendees, users)
    render
    expect(rendered).to have_selector("table")
    expect(rendered).to have_selector("table tr", count: users.size+1)
    expect(rendered).to have_content(users.first.first_name)
    expect(rendered).to have_content(users.first.food_preferences)
    expect(rendered).to have_content("membership")
  end

  it "renders a green row if user has membership" do
    user = create(:user, member: true)
    assign(:attendees, [user])
    render
    expect(rendered).to have_css(".success")
  end

  it "renders a red row if user does not have membership" do
    user = create(:user, member: false)
    assign(:attendees, [user])
    render
    expect(rendered).to have_css(".danger")
  end

end
