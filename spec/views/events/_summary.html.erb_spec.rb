require 'rails_helper'

RSpec.describe "events/_summary", type: :view do

  before :all do
    @event = build_stubbed(:event)
    assign(:event, @event)
  end

  it "renders a clickable link" do
    render
    expect(rendered).to have_link(:href => event_path(@event))
  end

  it "displays the title" do
    render
    expect(rendered).to have_content(@event.title)
  end

  it "displays the time of the event" do
    render
    expect(rendered).to have_content(format_time(@event.start_time))
  end

end
