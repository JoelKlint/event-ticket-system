require 'rails_helper'

RSpec.describe "users/set_ssn", type: :view do

  before :all do
    @user = build_stubbed(:user, ssn: nil)
    assign(:user, @user)
  end

  it "has a form" do
    render
    expect(rendered).to have_tag('form') do
      with_text_field "user[ssn]"
    end
  end

  it "has a form with destination save_ssn_user_path" do
    render
    expect(rendered).to have_tag('form', :with => { :action => save_ssn_user_path(@user) } )
  end

end
