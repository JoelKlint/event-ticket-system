require 'rails_helper'

RSpec.describe "users/show", type: :view do

  before :each do
    @user = create(:user, :with_tickets)
    sign_in @user
  end

  it "renders the events/summary partial" do
    render
    expect(rendered).to render_template(:partial => 'events/_summary')
  end

  it "renders the _form partial" do
    render
    expect(rendered).to render_template(:partial => 'users/_form')
  end

end
