require 'rails_helper'

RSpec.describe "users/_form", type: :view do

  before :all do
    @user = build_stubbed(:user)
    assign(:user, @user)
  end

  it "displays a form" do
    render
    expect(rendered).to have_tag('form')
  end

  it "displays a field for ssn" do
    render
    expect(rendered).to have_tag('form') do
      with_text_field "user[ssn]"
    end
  end

  it "displays fields for name" do
    render
    expect(rendered).to have_tag('form') do
      with_text_field "user[first_name]"
      with_text_field "user[last_name]"
    end
  end

  it "displays a field for food preferences" do
    render
    expect(rendered).to have_tag('form') do
      with_text_field "user[food_preferences]"
    end
  end

  it "displays a checkbox for membership status" do
    render
    expect(rendered).to have_tag('form') do
      with_checkbox "user[member]"
    end
  end

end
