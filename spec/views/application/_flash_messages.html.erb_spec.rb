require 'rails_helper'

RSpec.describe "application/_flash_messages", type: :view do
  it "does not render anything if flash is empty" do
    render
    expect(rendered).not_to match('.flash')
  end

  it "can render the info flash" do
    flash[:notice] = "test"
    render
    expect(rendered).to have_selector('.flash_notice')
  end

  it "can render the success flash" do
    flash[:success] = "test"
    render
    expect(rendered).to have_selector('.flash_success')
  end

  it "can render the error flash" do
    flash[:alert] = "test"
    render
    expect(rendered).to have_selector('.flash_alert')
  end
end
