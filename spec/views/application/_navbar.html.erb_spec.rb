require 'rails_helper'

RSpec.describe "application/_navbar", type: :view do

  it "should always have a link to the root url" do
    render
    expect(response).to have_link(:href => root_path)
  end

  context "logged out" do
    it "displays a link to log in" do
      render
      expect(response).to have_link("Log in", :href => new_user_session_path)
    end
  end

  context "logged in" do
    before :each do
      @user = create(:user)
      sign_in @user
    end

    it "displays a link to log out" do
      render
      expect(response).to have_link("Log out", :href => destroy_user_session_path)
    end

    it "displays a link to the user's info" do
      render
      expect(response).to have_link(:href => user_path(@user))
    end
  end

  context "admin" do
    before :each do
      @admin = create(:admin)
      sign_in @admin
    end

    it "displays a link to create a new event" do
      render
      expect(response).to have_link(:href => new_event_path)
    end

  end
end
