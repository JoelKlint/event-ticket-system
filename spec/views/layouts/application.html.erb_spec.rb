require 'rails_helper'

RSpec.describe "layouts/application", type: :view do
  it "should render the flash messages partial" do
    render
    expect(response).to render_template(:partial => 'application/_flash_messages')
  end

  it "should render the navbar partial" do
    render
    expect(response).to render_template(:partial => 'application/_navbar')
  end

  it "should not render the development_helper partial" do
    render
    expect(response).not_to render_template(:partial => 'application/_development_helper')
  end
end
