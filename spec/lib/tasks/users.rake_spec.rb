require 'rails_helper'

RSpec.describe "users:update_memberships", type: :rake do
  it "is a rake task" do
    expect(subject).to be_a(Rake::Task)
    expect(subject.name).to eq("users:update_memberships")
  end

  it "queues jobs to update membership status" do
    allow(MembershipService).to receive(:get_membership_status).and_return(true)
    user = create(:user, member: false)
    expect{subject.execute}.to have_enqueued_job(UpdateUserMembershipJob)
  end
end
