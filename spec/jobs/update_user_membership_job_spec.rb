require 'rails_helper'

RSpec.describe UpdateUserMembershipJob, type: :job do

  it "updates the member field if user became member" do
    user = create(:user, member: false)
    allow(MembershipService).to receive(:get_membership_status).and_return(true)
    expect{UpdateUserMembershipJob.perform_now(user)}
      .to change {user.member?}
      .from(false)
      .to(true)
  end

  it "updates the member field if user became a non member" do
    user = create(:user, member: true)
    allow(MembershipService).to receive(:get_membership_status).and_return(false)
    expect{UpdateUserMembershipJob.perform_now(user)}
      .to change {user.member?}
      .from(true)
      .to(false)
  end

  it "does not update the member field if membership is unchanged" do
    user = create(:user, member: true)
    allow(MembershipService).to receive(:get_membership_status).and_return(true)
    expect{UpdateUserMembershipJob.perform_now(user)}
      .not_to change {user.member?}
  end
end
