require 'rails_helper'

# Specs in this file have access to a helper object that includes
# the ApplicationHelper. For example:
#
# describe ApplicationHelper do
#   describe "string concat" do
#     it "concats two strings with spaces" do
#       expect(helper.concat_strings("this","that")).to eq("this that")
#     end
#   end
# end
RSpec.describe ApplicationHelper, type: :helper do
  context "format_time" do
    it "returns 'Undefined' for unvalid dates" do
      expect(helper.format_time(nil)).to eq('Undefined')
      expect(helper.format_time("String")).to eq('Undefined')
    end

    it "returns time in format 'date month year hour:minute'" do
      expect(helper.format_time(Time.now)).to eq(Time.now.strftime('%e %b %Y %H:%M'))
    end
  end

  it "fixes the current_user helper so it returns User.new if no one is logged in" do
    user = create(:user)
    expect(helper.current_user).to be_a(User)
    expect(helper.current_user).not_to eq(user)
    sign_in user
    expect(helper.current_user).to eq(user)
  end
end
