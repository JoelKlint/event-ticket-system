FactoryGirl.define do
  factory :event do
    title "Title"
    start_time Time.now
    end_time Time.now + 10
  end

  trait :with_attendees do
    transient do
      count 5
    end
    before(:create) do |event, evaluator|
      event.attendees = create_list(:user, evaluator.count, attending_events: [event])
    end
  end

  trait :invalid do
    title nil
    start_time nil
    end_time nil
  end
end
