FactoryGirl.define do
  factory :user do
    ssn { rand.to_s[2..11] }
    first_name "Generated"
    last_name "User"
    email { generate(:email) }
    password "d7a94h1oray89"
    food_preferences "Test food preferences"

    factory :admin do
      admin true
    end

  end

  trait :with_tickets do
    transient do
      count 5
    end
    before(:create) do |user, evaluator|
      user.attending_events = create_list(:event, evaluator.count, attendees: [user])
    end
  end

  factory :user_signup, class: User do
    email { generate :email }
    password "dsna780dnsa780"
    password_confirmation "dsna780dnsa780"
  end

  sequence :email do |n|
    "test#{n}@test.com"
  end

end
