require 'rails_helper'

RSpec.describe MembershipService do

  it "returns true for a ssn that is a member" do
    user = create(:user)
    is_member = MembershipService.get_membership_status(user)
    expect(is_member).to be true
  end

end
