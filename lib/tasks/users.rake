namespace :users do
  desc "update memership status for all users"
  task update_memberships: :environment do
    User.all.each do |user|
      UpdateUserMembershipJob.perform_later(user)
    end
  end

end
